CREATE DATABASE  IF NOT EXISTS `kurzy-krizik` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kurzy-krizik`;
-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: kurzy-krizik
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jazyk`
--

DROP TABLE IF EXISTS `jazyk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jazyk` (
  `idjazyk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  `zkratka` varchar(255) NOT NULL,
  PRIMARY KEY (`idjazyk`),
  UNIQUE KEY `idjazyk_UNIQUE` (`idjazyk`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`),
  UNIQUE KEY `zkratka_UNIQUE` (`zkratka`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jazyk`
--

LOCK TABLES `jazyk` WRITE;
/*!40000 ALTER TABLE `jazyk` DISABLE KEYS */;
INSERT INTO `jazyk` VALUES (1,'Český jazyk','CZE'),(2,'Anglický jazyk','ENG'),(3,'Ruský jazyk','RUS'),(4,'Neměcký jazyk','DEU'),(6,'Francouzský jazyk','FRA');
/*!40000 ALTER TABLE `jazyk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorie`
--

DROP TABLE IF EXISTS `kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorie` (
  `idkategorie` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  PRIMARY KEY (`idkategorie`),
  UNIQUE KEY `idkategorie_UNIQUE` (`idkategorie`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorie`
--

LOCK TABLES `kategorie` WRITE;
/*!40000 ALTER TABLE `kategorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurz`
--

DROP TABLE IF EXISTS `kurz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurz` (
  `idkurz` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  `delka` varchar(255) NOT NULL,
  `cena` varchar(255) NOT NULL,
  `popis` varchar(255) NOT NULL,
  `iduroven` int(10) unsigned NOT NULL,
  `idjazyk` int(10) unsigned NOT NULL,
  `idmisto` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idkurz`,`iduroven`,`idjazyk`,`idmisto`),
  UNIQUE KEY `idkurz_UNIQUE` (`idkurz`),
  KEY `fk_kurz_uroven1_idx` (`iduroven`),
  KEY `fk_kurz_jazyk1_idx` (`idjazyk`),
  KEY `fk_kurz_misto1_idx` (`idmisto`),
  CONSTRAINT `fk_kurz_jazyk1` FOREIGN KEY (`idjazyk`) REFERENCES `jazyk` (`idjazyk`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_misto1` FOREIGN KEY (`idmisto`) REFERENCES `misto` (`idmisto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_uroven1` FOREIGN KEY (`iduroven`) REFERENCES `uroven` (`iduroven`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='			';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurz`
--

LOCK TABLES `kurz` WRITE;
/*!40000 ALTER TABLE `kurz` DISABLE KEYS */;
/*!40000 ALTER TABLE `kurz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurz_kategorie`
--

DROP TABLE IF EXISTS `kurz_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurz_kategorie` (
  `idkurz` int(10) unsigned NOT NULL,
  `idkategorie` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idkurz`,`idkategorie`),
  KEY `fk_kurz_kategorie_kategorie1_idx` (`idkategorie`),
  KEY `fk_kurz_kategorie_kurz1_idx` (`idkurz`),
  CONSTRAINT `fk_kurz_kategorie_kategorie1` FOREIGN KEY (`idkategorie`) REFERENCES `kategorie` (`idkategorie`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_kategorie_kurz1` FOREIGN KEY (`idkurz`) REFERENCES `kurz` (`idkurz`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurz_kategorie`
--

LOCK TABLES `kurz_kategorie` WRITE;
/*!40000 ALTER TABLE `kurz_kategorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `kurz_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurz_uzivatel`
--

DROP TABLE IF EXISTS `kurz_uzivatel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurz_uzivatel` (
  `idkurz` int(10) unsigned NOT NULL,
  `iduroven` int(10) unsigned NOT NULL,
  `idjazyk` int(10) unsigned NOT NULL,
  `idmisto` int(10) unsigned NOT NULL,
  `iduzivatel` int(10) unsigned NOT NULL,
  `idlektor` int(10) unsigned NOT NULL,
  `konani` datetime NOT NULL,
  PRIMARY KEY (`idkurz`,`iduroven`,`idjazyk`,`idmisto`,`iduzivatel`,`idlektor`),
  KEY `fk_kurz_uzivatel_uzivatel1_idx` (`iduzivatel`),
  KEY `fk_kurz_uzivatel_kurz1_idx` (`idkurz`,`iduroven`,`idjazyk`,`idmisto`),
  KEY `fk_kurz_uzivatel_uzivatel2_idx` (`idlektor`),
  CONSTRAINT `fk_kurz_uzivatel_kurz1` FOREIGN KEY (`idkurz`, `iduroven`, `idjazyk`, `idmisto`) REFERENCES `kurz` (`idkurz`, `iduroven`, `idjazyk`, `idmisto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_uzivatel_uzivatel1` FOREIGN KEY (`iduzivatel`) REFERENCES `uzivatel` (`iduzivatel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_uzivatel_uzivatel2` FOREIGN KEY (`idlektor`) REFERENCES `uzivatel` (`iduzivatel`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurz_uzivatel`
--

LOCK TABLES `kurz_uzivatel` WRITE;
/*!40000 ALTER TABLE `kurz_uzivatel` DISABLE KEYS */;
/*!40000 ALTER TABLE `kurz_uzivatel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `misto`
--

DROP TABLE IF EXISTS `misto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misto` (
  `idmisto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  PRIMARY KEY (`idmisto`),
  UNIQUE KEY `idmisto_UNIQUE` (`idmisto`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misto`
--

LOCK TABLES `misto` WRITE;
/*!40000 ALTER TABLE `misto` DISABLE KEYS */;
/*!40000 ALTER TABLE `misto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `idrole` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  PRIMARY KEY (`idrole`),
  UNIQUE KEY `idrole_UNIQUE` (`idrole`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Administrátor'),(4,'Ban'),(2,'Manažer'),(3,'Uživatel');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uroven`
--

DROP TABLE IF EXISTS `uroven`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uroven` (
  `iduroven` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  PRIMARY KEY (`iduroven`),
  UNIQUE KEY `iduroven_UNIQUE` (`iduroven`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uroven`
--

LOCK TABLES `uroven` WRITE;
/*!40000 ALTER TABLE `uroven` DISABLE KEYS */;
/*!40000 ALTER TABLE `uroven` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uzivatel`
--

DROP TABLE IF EXISTS `uzivatel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzivatel` (
  `iduzivatel` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jmeno` varchar(255) NOT NULL,
  `prijmeni` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefon` varchar(255) NOT NULL,
  `heslo` varchar(255) NOT NULL,
  `idrole` int(10) unsigned NOT NULL,
  `datum_narozeni` date NOT NULL,
  PRIMARY KEY (`iduzivatel`,`idrole`),
  UNIQUE KEY `iduzivatel_UNIQUE` (`iduzivatel`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_uzivatel_role_idx` (`idrole`),
  CONSTRAINT `fk_uzivatel_role` FOREIGN KEY (`idrole`) REFERENCES `role` (`idrole`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uzivatel`
--

LOCK TABLES `uzivatel` WRITE;
/*!40000 ALTER TABLE `uzivatel` DISABLE KEYS */;
INSERT INTO `uzivatel` VALUES (1,'Vojtěch','Štengl','vojtech.stengl','vojtech.stengl@email.cz','778 636 835','855e4c2b5e067bf3e755e58c95e47bcb',3,'1933-05-01'),(2,'Eduard','Grundza','eduard.grundza','eduard.grundza@yahoo.com','883 223 183','855e4c2b5e067bf3e755e58c95e47bcb',3,'1999-06-25'),(3,'Michael','Hes','michael.hes','michael.hes@centrum.cz','298 223 609','855e4c2b5e067bf3e755e58c95e47bcb',3,'1920-09-19'),(4,'Rostislav','Pavel','rostislav.pavel','rostislav.pavel@email.com','608 712 297','855e4c2b5e067bf3e755e58c95e47bcb',3,'1960-02-13'),(5,'Ferdinand','Balek','ferdinand.balek','ferdinand.balek@email.com','725 724 190','855e4c2b5e067bf3e755e58c95e47bcb',3,'1979-11-23'),(6,'Kevin','Herink','kevin.herink','kevin.herink@volny.cz','719 671 901','855e4c2b5e067bf3e755e58c95e47bcb',3,'1949-09-26'),(7,'Eliáš','Hrubeš','elias.hrubes','elias.hrubes@centrum.cz','668 739 454','855e4c2b5e067bf3e755e58c95e47bcb',3,'1954-02-14'),(8,'Jindřich','Odvárka','jindrich.odvarka','jindrich.odvarka@email.com','877 423 224','855e4c2b5e067bf3e755e58c95e47bcb',3,'1901-01-24'),(9,'Matyas','Machek','matyas.machek','matyas.machek@centrum.cz','236 755 957','855e4c2b5e067bf3e755e58c95e47bcb',3,'1932-03-29'),(10,'Šimon','Horník','simon.hornik','simon.hornik@seznam.cz','760 281 164','855e4c2b5e067bf3e755e58c95e47bcb',3,'1910-04-24'),(11,'Jaroslav','Jurajda','jaroslav.jurajda','jaroslav.jurajda@seznam.cz','393 855 374','855e4c2b5e067bf3e755e58c95e47bcb',3,'1920-03-16'),(12,'Leoš','Brzobohatý','leos.brzobohaty','leos.brzobohaty@gmail.com','696 507 475','855e4c2b5e067bf3e755e58c95e47bcb',3,'1945-04-06'),(13,'Maxim','Kopa','maxim.kopa','maxim.kopa@email.com','663 192 899','855e4c2b5e067bf3e755e58c95e47bcb',3,'1969-07-21'),(14,'Karel','Klír','karel.klir','karel.klir@centrum.cz','945 384 622','855e4c2b5e067bf3e755e58c95e47bcb',3,'1933-04-29'),(15,'Richard','Saidl','richard.saidl','richard.saidl@volny.cz','891 795 770','855e4c2b5e067bf3e755e58c95e47bcb',3,'1901-01-24'),(16,'Igor','Mates','igor.mates','igor.mates@gmail.com','280 376 524','855e4c2b5e067bf3e755e58c95e47bcb',3,'1932-03-29'),(17,'Marian','Vimr','marian.vimr','marian.vimr@email.cz','297 627 217','855e4c2b5e067bf3e755e58c95e47bcb',3,'1910-04-24'),(18,'Marco','Tomšík','marco.tomsik','marco.tomsik@seznam.cz','333 695 422','855e4c2b5e067bf3e755e58c95e47bcb',3,'1920-03-16'),(19,'Josef','Macko','josef.macko','josef.macko@seznam.cz','308 452 945','855e4c2b5e067bf3e755e58c95e47bcb',3,'1912-06-17'),(20,'Kristián','Bouda','kristian.bouda','kristian.bouda@volny.cz','731 211 147','855e4c2b5e067bf3e755e58c95e47bcb',3,'1934-12-19'),(21,'Pavla','Stryková','pavla.strykova','pavla.strykova@volny.cz','792 819 287','855e4c2b5e067bf3e755e58c95e47bcb',3,'1979-06-24'),(22,'Jessica','Carbolová','jessica.carbolova','jessica.carbolova@seznam.cz','971 701 961','855e4c2b5e067bf3e755e58c95e47bcb',3,'1971-09-24'),(23,'Růžena','Opálková','ruzena.opalkova','ruzena.opalkova@centrum.cz','221 652 314','855e4c2b5e067bf3e755e58c95e47bcb',3,'1912-07-21'),(24,'Iva','Bubeníčková','iva.bubenickova','iva.bubenickova@volny.cz','384 670 779','855e4c2b5e067bf3e755e58c95e47bcb',3,'1934-02-23'),(25,'Eleanor','Kratochvilová','eleanor.kratochvilova','eleanor.kratochvilova@gmail.com','595 917 902','855e4c2b5e067bf3e755e58c95e47bcb',3,'1952-12-01'),(26,'Elena','Matuchová','elena.matuchova','elena.matuchova@yahoo.com','407 412 918','855e4c2b5e067bf3e755e58c95e47bcb',3,'1919-05-14'),(27,'Eva','Semelová','eva.semelova','eva.semelova@yahoo.com','395 517 394','855e4c2b5e067bf3e755e58c95e47bcb',3,'1991-04-19'),(28,'Emílie','Matějová','emilie.matejova','emilie.matejova@yahoo.com','455 894 450','855e4c2b5e067bf3e755e58c95e47bcb',3,'1903-07-20'),(29,'Týna','Roztočilová','tyna.roztocilova','tyna.roztocilova@seznam.cz','815 315 611','855e4c2b5e067bf3e755e58c95e47bcb',3,'1965-07-23'),(30,'Anastázie','Lipková','anastazie.lipkova','anastazie.lipkova@email.com','958 969 792','855e4c2b5e067bf3e755e58c95e47bcb',3,'1939-04-20'),(31,'Emily','Brunová','emily.brunova','emily.brunova@volny.cz','693 571 476','855e4c2b5e067bf3e755e58c95e47bcb',3,'1965-05-02'),(32,'Olga','Křenová','olga.krenova','olga.krenova@centrum.cz','582 835 109','855e4c2b5e067bf3e755e58c95e47bcb',3,'1993-07-14'),(33,'Anna Marie','Vítová','anna marie.vitova','anna marie.vitova@seznam.cz','414 427 446','855e4c2b5e067bf3e755e58c95e47bcb',3,'1959-11-25'),(34,'Johana','Slováček','johana.slovacek','johana.slovacek@gmail.com','763 494 271','855e4c2b5e067bf3e755e58c95e47bcb',3,'1963-06-24'),(35,'Samantha','Kamarádová','samantha.kamaradova','samantha.kamaradova@email.com','539 301 891','855e4c2b5e067bf3e755e58c95e47bcb',3,'1995-09-09'),(36,'Dita','Štefánková','dita.stefankova','dita.stefankova@email.cz','373 550 888','855e4c2b5e067bf3e755e58c95e47bcb',3,'1999-12-13'),(37,'Natalie','Picková','natalie.pickova','natalie.pickova@email.cz','475 696 187','855e4c2b5e067bf3e755e58c95e47bcb',3,'1965-11-23'),(38,'Elen','Trunečková','elen.truneckova','elen.truneckova@seznam.cz','968 410 607','855e4c2b5e067bf3e755e58c95e47bcb',3,'1973-05-26'),(39,'Anežka','Stanovská','anezka.stanovska','anezka.stanovska@centrum.cz','947 909 775','855e4c2b5e067bf3e755e58c95e47bcb',3,'1999-07-04'),(40,'Natálie','Měřičková','natalie.merickova','natalie.merickova@seznam.cz','805 859 695','855e4c2b5e067bf3e755e58c95e47bcb',3,'1965-07-11');
/*!40000 ALTER TABLE `uzivatel` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-09 20:18:39
