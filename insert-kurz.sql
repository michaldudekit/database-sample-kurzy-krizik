-- KURZ
insert into kurz(nazev, delka, cena, popis, iduroven, idjazyk, idmisto) values
('Základní práce s PC', '30 minut', '175 Kč', 'Seznámení s základní manipulaci počítače.', 1, (select idjazyk from jazyk WHERE zkratka='CZE'), (select idmisto from misto WHERE nazev='Praha 1')),
('Plynulost v angličtině', '60 minut', '225 Kč', 'Naučíme vás plynule mluvit anglicky. Předpokládá se předchozí znalost anglického jazyka.', 2, (select idjazyk from jazyk WHERE zkratka='CZE'), (select idmisto from misto WHERE nazev='Brno')),
('Plavání', '90 minut', '300 Kč', 'Naučíme vás, jak se neutopit.', 1, (select idjazyk from jazyk WHERE zkratka='CZE'), (select idmisto from misto WHERE nazev='Ostrava')),
('Zdokonalení vaření', '60 minut', '500 Kč', 'Naučíme vás jak vařit jako profesionál. Předpokládá se, že už budete umět vařit.', 3, (select idjazyk from jazyk WHERE zkratka='CZE'), (select idmisto from misto WHERE nazev='Praha 1')),
('Starání se o domácí mazlíčky', '15 minut', '100 Kč', 'Naučíme vás vše co potřebujete vědět, než si koupíte mazlíčka', 1, (select idjazyk from jazyk WHERE zkratka='CZE'), (select idmisto from misto WHERE nazev='Ostrava'));
insert into kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) values
('Angličtina','45 min','70 kč/hodinu','Zde se naučite anglicky',1,2,1),
('Vaření','100 min','210 kč/lekci','Zde se naučíte vařit',3,1,1),
('Joga','60 min','80 kč/hodinu','Zde se naučite jogu',2,1,1),
('Autoškola','40 min teorie, 100 minut jízdy','12 000 kč za celý kurz','Zde se naučite řídit',1,1,2),
('Plavání','50 min','60 kč/lekci','Zde se naučite plavat',3,1,2);
INSERT INTO kurz( nazev,delka, cena, popis, iduroven, idjazyk, idmisto) VALUES
 ( 'Zaklady Virtualizace', '2h', '500 kc', ' vsechny zaklady pro virtualizaci ', '1', '1', '1'),
 ( 'Latina hrou', '2h', '700', 'V tom to kurzu se naucime latinu :) ', '2', '1', '2'),
 ( 'Florbal', '2h', '400', 'Miluješ florbal pojd k nám', '3', '1','3'),
 ( 'Koláče', '2h', '250', 'zde se naucime pec vyborne kolace', '2', '2','2'),
 ( 'Péče o papoušky', '2h', '300', 'Zde se dozvíte vse o chovu papoušků', '2', '1','1');
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ('Hrátky s excelem', '90 min', '450Kč', 'Na tomto kurzu se naučíte pracovat s excelem', '4', '1', '1');
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ('Němčina hrou', '60 min', '500Kč', 'Na tomto kurzu vás naučíme plyně komunikovat v Němčině', '5', '1', '2');
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ('Šach Mat', '60 min', '300Kč', 'Na tomto kurzu se naučíte hrát proti opravdovým mistrům', '6', '1', '4');
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ('Oregáno', '120min', '600Kč', 'Na tomto kurzu se naučíme základní práci se zeleninou', '4', '1', '3');
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ('Ohrádka', '90min', '800Kč', 'Na tomto kurzu si řekneme jak začít s chovem prasátek', '4', '1', '5');
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) values ("Základy Inventoru","60 minut","200kč","Kurz pro začátečníky s Inventorem","1","6","1");
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) values ("Gymnastika u Filipa","45 minut","250kč","Naučíme tě se točit na hrazdě","2","4","3");
INSERT INTO kurz(nazev, delka, cena, popis, iduroven, idjazyk, idmisto) VALUES
('Resolve', 120, 5500, 'Základy v DaVinci Resolve',1,1,1),
('Fotbal', 90, 1500, 'Střelba na bránu.',1 ,1,1),
('Španělština', 45, 1000, 'Základy ŠJ',1,1,1),
('Buřty na pivě', 40, 2000, 'Naučte se nejlepší Buřty na pivě.',1,1,1),
('Pes', 120, 500, 'Výcvik',1,1,1);
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ("Učení Powerpointu", "120 min", "250Kč", "Základní funkce Powerpointu", "1", "1", "1");
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ("Anglický jazyk", "90 min", "200Kč", "Angličtina pro pokročilé", "2", "2", "1");
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ("Atletika!", "90 min", "100Kč", "Atletická průprava", "2", "1", "2");
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ("Škola Vaření Brutus", "150Kč", "120 min", "Vaření s generálem Brutusem", "3", "1", "3");
INSERT INTO kurz (nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES ("Farmaření Honzík","100Kč","180 min","Odpoledne se zvířátky", "1", "1", "4");
insert into kurz values (1, "IT", "2 hod.", "335 kc/hod", "Sovorek IT daser", 1, 7, 3),
(2, "Komunikace", "1 hod.", "245 kc/hod", "Naucte se mluvit", 1, 1, 4),
(3, "Sport and fitness", "1 hod.", "435 kc/hod", "Excercise", 1, 3, 3),
(4, "Kulinariya", "2 hod.", "1000 kc/hod", "Nauchim vas gotovit yedu", 3, 3, 7),
(5, "Zpev", "10 hod.", "20 kc/hod", "No preci, kazdy umi zpivat, to neni tak tezky", 1, 6, 6);
insert into kurz(nazev, delka, cena, popis, iduroven, idjazyk, idmisto) values
('Vnimani sily 101', '1 hodina', '200 Kč', 'Co je to sila.', 1, 1, 1),
('Zacatky jedaismu', '2 hodiny', '350 Kč', 'Uvod do jedaismu.', 1, 1, 2),
('LIGHTSABER 101', '1 hodiny', '500 Kč', 'Zakldy se svetelnym mecem.', 2, 1, 2),
('Force duels', '5 hodin', '1500 Kč', 'Kontrolovane zapasy s nasim jedi mastrem', 3, 2, 3),
('Vytvor si svuj vlastni svetelny mec', '3 dny', '10000 Kč', 'Sestroj si vlastni svetelny mec', 3, 1, 3);
INSERT INTO kurz(nazev, delka, cena, popis, iduroven, idjazyk, idmisto) VALUES
('SwiftUI', 60, 1500, 'Kurz, ve kterém se naučíte základy SwiftUI',1,1,1),
('Fotball', 120, 1000, 'Kurz, ve kterém se naučíte přihrávat',1 ,1,1),
('Angličtina', 180, 2500, 'Doučování',1,1,1),
('Svíčková', 180, 100, 'Naučíte se vařit svíčkovou',1,1,1),
('Výcvik psa', 180, 2500, 'Výcvik',1,1,1);
INSERT INTO kurz(nazev,delka,cena,popis,iduroven,idjazyk,idmisto) VALUES
('Java', '45 min', 200, 'Kurz programování v Javě',1,1,1),
('Volejbal', '120 min', 1000, 'Naučíme vás hrát volejbal',1 ,1,1),
('Finština', '45 min', 300, 'Puhumme suomea',1,1,1),
('Pečení', '120 min', 500, 'Zde vás budeme učit jak péct',1,1,1),
('Kočky', '60 min', 1000, 'Jak správně vycvičit svoji kočku',1,1,1);

select * from kurz;