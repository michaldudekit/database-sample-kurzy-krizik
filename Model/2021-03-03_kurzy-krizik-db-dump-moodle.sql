CREATE DATABASE  IF NOT EXISTS `kurzy-krizik` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kurzy-krizik`;
-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: kurzy-krizik
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jazyk`
--

DROP TABLE IF EXISTS `jazyk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jazyk` (
  `idjazyk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  `zkratka` varchar(255) NOT NULL,
  PRIMARY KEY (`idjazyk`),
  UNIQUE KEY `idjazyk_UNIQUE` (`idjazyk`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`),
  UNIQUE KEY `zkratka_UNIQUE` (`zkratka`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jazyk`
--

LOCK TABLES `jazyk` WRITE;
/*!40000 ALTER TABLE `jazyk` DISABLE KEYS */;
INSERT INTO `jazyk` VALUES (1,'Český jazyk','CZE'),(2,'Anglický jazyk','ENG'),(3,'Ruský jazyk','RUS'),(4,'Neměcký jazyk','DEU'),(6,'Francouzský jazyk','FRA');
/*!40000 ALTER TABLE `jazyk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorie`
--

DROP TABLE IF EXISTS `kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorie` (
  `idkategorie` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  PRIMARY KEY (`idkategorie`),
  UNIQUE KEY `idkategorie_UNIQUE` (`idkategorie`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorie`
--

LOCK TABLES `kategorie` WRITE;
/*!40000 ALTER TABLE `kategorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurz`
--

DROP TABLE IF EXISTS `kurz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurz` (
  `idkurz` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  `delka` varchar(255) NOT NULL,
  `cena` varchar(255) NOT NULL,
  `popis` varchar(255) NOT NULL,
  `iduroven` int(10) unsigned NOT NULL,
  `idjazyk` int(10) unsigned NOT NULL,
  `idmisto` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idkurz`,`iduroven`,`idjazyk`,`idmisto`),
  UNIQUE KEY `idkurz_UNIQUE` (`idkurz`),
  KEY `fk_kurz_uroven1_idx` (`iduroven`),
  KEY `fk_kurz_jazyk1_idx` (`idjazyk`),
  KEY `fk_kurz_misto1_idx` (`idmisto`),
  CONSTRAINT `fk_kurz_jazyk1` FOREIGN KEY (`idjazyk`) REFERENCES `jazyk` (`idjazyk`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_misto1` FOREIGN KEY (`idmisto`) REFERENCES `misto` (`idmisto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_uroven1` FOREIGN KEY (`iduroven`) REFERENCES `uroven` (`iduroven`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='			';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurz`
--

LOCK TABLES `kurz` WRITE;
/*!40000 ALTER TABLE `kurz` DISABLE KEYS */;
/*!40000 ALTER TABLE `kurz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurz_kategorie`
--

DROP TABLE IF EXISTS `kurz_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurz_kategorie` (
  `idkurz` int(10) unsigned NOT NULL,
  `idkategorie` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idkurz`,`idkategorie`),
  KEY `fk_kurz_kategorie_kategorie1_idx` (`idkategorie`),
  KEY `fk_kurz_kategorie_kurz1_idx` (`idkurz`),
  CONSTRAINT `fk_kurz_kategorie_kategorie1` FOREIGN KEY (`idkategorie`) REFERENCES `kategorie` (`idkategorie`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_kategorie_kurz1` FOREIGN KEY (`idkurz`) REFERENCES `kurz` (`idkurz`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurz_kategorie`
--

LOCK TABLES `kurz_kategorie` WRITE;
/*!40000 ALTER TABLE `kurz_kategorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `kurz_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurz_uzivatel`
--

DROP TABLE IF EXISTS `kurz_uzivatel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurz_uzivatel` (
  `idkurz` int(10) unsigned NOT NULL,
  `iduroven` int(10) unsigned NOT NULL,
  `idjazyk` int(10) unsigned NOT NULL,
  `idmisto` int(10) unsigned NOT NULL,
  `iduzivatel` int(10) unsigned NOT NULL,
  `idlektor` int(10) unsigned NOT NULL,
  `konani` datetime NOT NULL,
  PRIMARY KEY (`idkurz`,`iduroven`,`idjazyk`,`idmisto`,`iduzivatel`,`idlektor`),
  KEY `fk_kurz_uzivatel_uzivatel1_idx` (`iduzivatel`),
  KEY `fk_kurz_uzivatel_kurz1_idx` (`idkurz`,`iduroven`,`idjazyk`,`idmisto`),
  KEY `fk_kurz_uzivatel_uzivatel2_idx` (`idlektor`),
  CONSTRAINT `fk_kurz_uzivatel_kurz1` FOREIGN KEY (`idkurz`, `iduroven`, `idjazyk`, `idmisto`) REFERENCES `kurz` (`idkurz`, `iduroven`, `idjazyk`, `idmisto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_uzivatel_uzivatel1` FOREIGN KEY (`iduzivatel`) REFERENCES `uzivatel` (`iduzivatel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurz_uzivatel_uzivatel2` FOREIGN KEY (`idlektor`) REFERENCES `uzivatel` (`iduzivatel`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurz_uzivatel`
--

LOCK TABLES `kurz_uzivatel` WRITE;
/*!40000 ALTER TABLE `kurz_uzivatel` DISABLE KEYS */;
/*!40000 ALTER TABLE `kurz_uzivatel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `misto`
--

DROP TABLE IF EXISTS `misto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misto` (
  `idmisto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  PRIMARY KEY (`idmisto`),
  UNIQUE KEY `idmisto_UNIQUE` (`idmisto`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misto`
--

LOCK TABLES `misto` WRITE;
/*!40000 ALTER TABLE `misto` DISABLE KEYS */;
/*!40000 ALTER TABLE `misto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `idrole` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  PRIMARY KEY (`idrole`),
  UNIQUE KEY `idrole_UNIQUE` (`idrole`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Administrátor'),(4,'Ban'),(2,'Manažer'),(3,'Uživatel');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uroven`
--

DROP TABLE IF EXISTS `uroven`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uroven` (
  `iduroven` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  PRIMARY KEY (`iduroven`),
  UNIQUE KEY `iduroven_UNIQUE` (`iduroven`),
  UNIQUE KEY `nazev_UNIQUE` (`nazev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uroven`
--

LOCK TABLES `uroven` WRITE;
/*!40000 ALTER TABLE `uroven` DISABLE KEYS */;
/*!40000 ALTER TABLE `uroven` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uzivatel`
--

DROP TABLE IF EXISTS `uzivatel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzivatel` (
  `iduzivatel` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jmeno` varchar(255) NOT NULL,
  `prijmeni` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefon` varchar(255) NOT NULL,
  `heslo` varchar(255) NOT NULL,
  `idrole` int(10) unsigned NOT NULL,
  `datum_narozeni` date NOT NULL,
  PRIMARY KEY (`iduzivatel`,`idrole`),
  UNIQUE KEY `iduzivatel_UNIQUE` (`iduzivatel`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_uzivatel_role_idx` (`idrole`),
  CONSTRAINT `fk_uzivatel_role` FOREIGN KEY (`idrole`) REFERENCES `role` (`idrole`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uzivatel`
--

LOCK TABLES `uzivatel` WRITE;
/*!40000 ALTER TABLE `uzivatel` DISABLE KEYS */;
INSERT INTO `uzivatel` VALUES (1,'Vojtěch','Štengl','vojtech.stengl','vojtech.stengl@email.cz','778 636 835','855e4c2b5e067bf3e755e58c95e47bcb',3,'1933-05-01');
/*!40000 ALTER TABLE `uzivatel` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-09 20:18:39
